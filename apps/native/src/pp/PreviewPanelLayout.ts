import { makeAutoObservable } from "mobx";
import { Rect } from "@relut/ui";
import type { IPanelLayout } from "@relut/ui";
import type { PreviewSource } from "./PreviewSource";

export class PreviewPanelLayoutTop implements IPanelLayout {
  private toolbarHeight = 36;
  rect: Rect = new Rect(0, 0, 100, 100);

  constructor(private source: PreviewSource) {
    makeAutoObservable(this);
  }

  update(rect: Rect) {
    this.rect = rect;
    this.source.leftPanel?.resizeImage();
    this.source.rightPanel?.resizeImage();
  }

  get canvasRect(): Rect {
    return new Rect(
      0,
      this.toolbarHeight,
      this.rect.width,
      this.rect.height - this.toolbarHeight,
    );
  }

  get leftRect() {
    const rect = this.singleRect;
    return new Rect(rect.x, rect.y, rect.width / 2, rect.height);
  }

  get rightRect() {
    const rect = this.singleRect;
    return new Rect(
      rect.x + rect.width / 2,
      rect.y,
      rect.width / 2,
      rect.height,
    );
  }

  get singleRect() {
    return new Rect(
      0,
      0,
      this.rect.width,
      this.rect.height - this.toolbarHeight,
    );
  }

  get toolbarRect(): Rect {
    return new Rect(0, 0, this.rect.width, this.toolbarHeight);
  }
}

export class PreviewPanelLayoutLeft implements IPanelLayout {
  private toolbarHeight = 36;
  rect: Rect = new Rect(0, 0, 100, 100);

  constructor(private source: PreviewSource) {
    makeAutoObservable(this);
  }

  update(rect: Rect) {
    this.rect = rect;
    this.source.leftPanel?.resizeImage();
    this.source.rightPanel?.resizeImage();
  }

  get canvasRect(): Rect {
    return new Rect(
      this.toolbarHeight,
      0,
      this.rect.width - this.toolbarHeight,
      this.rect.height,
    );
  }

  get leftRect() {
    const rect = this.singleRect;
    return new Rect(0, 0, rect.width / 2, rect.height);
  }

  get rightRect() {
    const rect = this.singleRect;
    return new Rect(rect.width / 2, 0, rect.width / 2, rect.height);
  }

  get singleRect() {
    return new Rect(
      0,
      0,
      this.rect.width - this.toolbarHeight,
      this.rect.height,
    );
  }

  get toolbarRect(): Rect {
    return new Rect(0, 0, this.toolbarHeight, this.rect.height);
  }
}
