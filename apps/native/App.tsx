import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Button } from '@repo/ui';
import {
  ButtonGroup,
  ButtonSimple,
  DragInput,
  PreviewPanel,
  PreviewPanelComponent,
} from '@relut/ui';
import { PreviewSource } from './src/pp/PreviewSource';
import { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Matrix } from './src/pp/Matrix';

const Native = observer(() => {
  if (1) {
    return (
      <View style={styles.container}>
        <Matrix />
      </View>
    );
  }

  const [model, modelSet] = useState<PreviewPanel | null>(null);

  useEffect(() => {
    if (!model) {
      const source = new PreviewSource();
      const model = new PreviewPanel(source);
      model.source.changeZoomMode?.(true);
      modelSet(model);
    }
  }, []);

  return model ? (
    <PreviewPanelComponent
      model={model}
      style={[
        model.layout.rect.layoutObject,
        { width: '100%', height: '100%' },
      ]}
    />
  ) : null;
});

export default Native;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#555',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 20,
    fontSize: 36,
  },
  input: {
    width: 100,
    // fontSize: 20,
  },
});
