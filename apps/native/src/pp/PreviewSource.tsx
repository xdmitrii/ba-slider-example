import React, { type ReactElement } from 'react';
import { action, makeAutoObservable, observable } from 'mobx';
import {
  PreviewPanelLayoutLeft,
  PreviewPanelLayoutTop,
} from './PreviewPanelLayout';
import {
  PreviewZoomControls,
  PreviewRenderUnit,
  EZoomMode,
  DragInput,
} from '@relut/ui';
import { Image, View, StyleSheet } from 'react-native';
import type { ViewProps } from 'react-native';
import type { IPanelSource, IPanelLayout, IPanelZoomControls } from '@relut/ui';
import type { PreviewPanel } from '@relut/ui';

// typescript trick to allow `wheel` events in react-native
interface CustomViewProps extends ViewProps {
  onWheel?: any;
}
const CustomView = View as any as React.FC<CustomViewProps>;

export class PreviewSource implements IPanelSource {
  readonly layout: IPanelLayout;
  readonly zoom: IPanelZoomControls;
  leftPanel: PreviewRenderUnit;
  rightPanel: PreviewRenderUnit;
  linked = false;
  panEnabled = false;
  images;
  //   leftImage: PreviewImage;
  //   rightImage: PreviewImage;

  constructor() {
    this.layout = new PreviewPanelLayoutTop(this);
    const size = { width: 300, height: 300 };
    this.leftPanel = new PreviewRenderUnit(this, 0);
    this.rightPanel = new PreviewRenderUnit(this, 1);
    this.leftPanel.updateImageSize(size);
    this.rightPanel.updateImageSize(size);
    this.leftPanel.setZoomMode(EZoomMode.CUSTOM);
    this.zoom = new PreviewZoomControls(this, {
      label: 'Zoom',
      min: 10,
      max: 100,
      step: 1,
    });
    this.images = ['/original.jpeg', '/graded.png'];
    // this.leftImage = new PreviewImage(this.images[0], 0, this);
    // this.rightImage = new PreviewImage(this.images[1], 1, this);

    makeAutoObservable(this, {
      layout: observable.ref,
      setLinked: action.bound,
      setMovable: action.bound,
    });
  }

  updateImageSource(index: 0 | 1, uri: string) {
    this.images[index] = uri;
    // if (index === 0) {
    //   this.leftImage.updateImageSource(uri);
    // } else {
    //   this.rightImage.updateImageSource(uri);
    // }
  }

  setMovable(movable: boolean) {
    this.panEnabled = movable;
  }

  setLinked(linked: boolean) {
    this.linked = linked;
    // if (
    //   this.leftImage.size.width === this.rightImage.size.width &&
    //   this.leftImage.size.height === this.rightImage.size.height
    // ) {
    // } else {
    //   this.linked = false;
    // }
  }

  changeZoomMode(fill: boolean) {
    if (fill) {
      this.leftPanel.setZoomMode(EZoomMode.FILL);
      this.rightPanel.setZoomMode(EZoomMode.FILL);
    } else {
      this.leftPanel.setZoomMode(EZoomMode.FIT);
      this.rightPanel.setZoomMode(EZoomMode.FIT);
    }
  }

  renderByIndex(index: 0 | 1): ReactElement {
    const panel = index === 0 ? this.leftPanel : this.rightPanel;
    // const image = index === 0 ? this.leftImage : this.rightImage;

    console.log(panel.styles);
    return (
      <CustomView
        {...panel.panHandlers}
        style={[StyleSheet.absoluteFill, styles.panel]}
        // onWheel={(e) => panel.handleWheelZoom(e)}
      >
        <View
          style={[
            panel.styles,
            {
              borderWidth: 1,
              borderColor: 'black',
              backgroundColor: 'red',
            },
          ]}
        />
      </CustomView>
    );
  }
}

const styles = StyleSheet.create({
  panel: {
    overflow: 'hidden',
    backgroundColor: '#333',
  },
  fill: {
    width: '100%',
    height: '100%',
  },
});
