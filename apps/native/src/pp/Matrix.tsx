import { View } from 'react-native';

export const Matrix = () => {
  return (
    <View
      style={[
        {
          backgroundColor: 'red',
          width: 200,
          height: 200,
          transform: [
            { scale: 2 },
            { rotate: '10deg' },
            // { translateX: 200 },
            // { matrix: [2, 100, 0, 550, 12, 0, 0, 0, 1] },
            // { matrix: [2, 0, 0, 1, 200, 1, 0, 0, 0] },
          ],
        },
      ]}
    />
  );
};
