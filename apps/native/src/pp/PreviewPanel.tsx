import { PreviewPanel as PP, PreviewPanelComponent } from '@relut/ui';
import React from 'react';
import { PreviewSource } from './PreviewSource';

export const PreviewPanel = () => {
  const source = new PreviewSource();
  const model = new PP(source);

  return (
    <PreviewPanelComponent
      model={model}
      style={[
        model.layout.rect.layoutObject,
        { width: '100%', height: '100%' },
      ]}
    />
  );
};
